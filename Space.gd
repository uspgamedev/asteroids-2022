extends Node2D

export var numberOfAsteroids = 1
var smallAsteroid = preload("res://characters/asteroids/Small_asteroid.tscn")
var mediumAsteroid = preload("res://characters/asteroids/Medium_asteroid.tscn")
var largeAsteroid = preload("res://characters/asteroids/Large_asteroid.tscn")
var bullet = preload("res://characters/player/Bullet.tscn")
var asteroids_left = 0
var score = 0


func _ready():
	spawn_wave()

func getRandomPosition():
	randomize()
	var randomPosition = Vector2(rand_range(0, 1024), rand_range(0, 600))
	return randomPosition

func _process(delta):
	if Input.is_action_just_pressed("shoot"):
		var new_bullet = bullet.instance()
		new_bullet.position = $Player_Ship.position
		new_bullet.rotation = $Player_Ship.rotation
		add_child(new_bullet)

func _on_asteroid_destroyed(global_position):
	score += 1
	$CanvasLayer/Score_label.text = "Score: " + str(score)
	asteroids_left -= 1
	if asteroids_left == 0:
		numberOfAsteroids += 1
		spawn_wave()

func spawn_wave():
	for i in numberOfAsteroids:
		var small_Asteroid = smallAsteroid.instance()
		small_Asteroid.position = getRandomPosition()
		small_Asteroid.connect("asteroid_destroyed", self, "_on_asteroid_destroyed")
		add_child(small_Asteroid)
		asteroids_left += 1
		
		var medium_Asteroid = mediumAsteroid.instance()
		medium_Asteroid.position = getRandomPosition()
		medium_Asteroid.connect("asteroid_destroyed", self, "_on_asteroid_destroyed")
		add_child(medium_Asteroid)
		asteroids_left += 1
		
		var large_Asteroid = largeAsteroid.instance()
		large_Asteroid.position = getRandomPosition()
		large_Asteroid.connect("asteroid_destroyed", self, "_on_asteroid_destroyed")
		add_child(large_Asteroid)
		asteroids_left += 1


func _on_Player_Ship_killed():
	$time_to_restart.start()


func _on_time_to_restart_timeout():
	get_tree().change_scene("res://Game_over.tscn")
