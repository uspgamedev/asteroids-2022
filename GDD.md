# Game Design Document (GDD)
Documento no qual anotamos todas as mecânicas, elementos, regras e interações do jogo.
## Elementos
- A nave que o jogador controla; 
- Asteroides que se movimentam pela tela;
<br></br>
## Regras e interações
- Se a nave do jogador é destruída, o jogo acaba.
- O jogador deve destruir os asteroides 
<br></br>
## Mecânicas
### Em relação à destruição da nave do jogador
- Se um asteroide encostar na nave do jogador, ela é destruída;
### Em relação à nave do jogador
- O jogador pode mudar a direção da nave usando as setas para esquerda e para direita.
- A seta para cima acelera a nave do jogador na direção para onde está apontada.
- Ao acelerar, a nave faz um movimento uniforme, ou seja, ficará andando na mesma direção até que o jogador a acelere em outra direção ou até que seja destruída.
- Caso a nave chegue a algum limite da tela, ela reaparece a 180º em relação a onde estava, tanto horizontalmente como verticalmente. Ou seja, se a nave chegar no limite da esquerda, ela reaparece na direita; se ela chegar no limite superior, ela reaparece na parte de baixo. Em todos os casos, a nave conserva sua velocidade.
- A nave tem uma velocidade máxima de 300 px/s.
- Para disparar os tiros, o jogador deve apertar a tecla barra de espaço
### Em relação aos asteroides
- Quando um asteroide é destruído, ele se transforma em dois menores (a menos que já esteja no tamanho mínimo).
- Os asteroides nascem com diferentes tamanhos e com velocidade em uma direção aleatória.
<br></br>
## Sprites
- [X] Nave do jogador
- [X] Asteroide grande
- [X] Asteroide médio
- [X] Asteroide pequeno
- [ ] Espaço (fundo)
