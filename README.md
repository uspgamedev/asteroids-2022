# Asteroids 2022
Este projeto está sendo desenvolvido como parte do treinamento oferecido pelo USPGameDev, um grupo de pesquisa e desenvolvimento de jogos criado por alunos da Universidade de São Paulo (USP).<br></br>
O principal objetivo é introduzir aos estudantes ideias e conceitos sobre desenvolvimento de jogos. Para isso, utilizarão a engine 'Godot' e a ferramenta de versionamento 'Git' para recriar o popular jogo arcade 'Asteroids', lançado para Atari em 1979.

> Status: Em desenvolvimento!

## Estão colaborando:
- Augusto Lima Jardim, estudante de Astronomia (IAG-USP)
- Heloisa, estudante de editoração (ECA-USP)

## Tecnologias utilizadas:

Tecnologia | Versão
:-------:  | :------:
Godot      |   3.4.3
Git        |   2.35.1

## Descrição:
O jogo tem como elementos uma nave do jogador, que pode disparar projéteis, e asteroides de tamanhos diferentes. O objetivo do jogo é destruir o maior número possível de asteroides. Se algum asteroide encostar na nave do jogador, o jogo acaba.


## Como rodar o programa:
Baixe os arquivos para o seu computador (não disponível para mobile) e abra-o com a engine Godot. <br></br>
Depois disso, aperte no botão 'Rodar' na parte superior da tela, ou então utilize o atalho F5 para rodar o programa.
