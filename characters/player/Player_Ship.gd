extends Area2D

export var rotate_speed = 4
var velocity = Vector2.ZERO
var thrust = 0
signal player_killed

func _process(delta):
	var rot = Input.get_axis("ui_left","ui_right") * rotate_speed
	rotate(rot * delta)
	
	thrust = Input.get_action_strength("ui_up")
	velocity += transform.x * thrust * 10
	velocity = lerp(velocity, Vector2.ZERO, delta)
	position += velocity * delta
	teleportOnScreen()
	
func teleportOnScreen():
	if position.x < 0:
		position.x = 1024
	if position.x > 1024:
		position.x = 0
	if position.y < 0:
		position.y = 600
	if position.y > 600:
		position.y = 0


func _on_Player_Ship_area_entered(area):
	emit_signal("player_killed")
	queue_free()
