extends Area2D

class_name Asteroid

var velocity = Vector2.ZERO 

signal asteroid_destroyed

func _ready():
	randomize()
	var x = rand_range(-100, 100)
	var y = rand_range(-100, 100)
	velocity = Vector2(x,y)

func _process(delta):
	position += velocity * delta
	teleportOnScreen()

func teleportOnScreen():
	if position.x < 0:
		position.x = 1024
	if position.x > 1024:
		position.x = 0
	if position.y < 0:
		position.y = 600
	if position.y > 600:
		position.y = 0


func _on_Large_asteroid_area_entered(area):
	if area is Bullet:
		emit_signal("asteroid_destroyed", global_position)
		area.queue_free()
		queue_free()

func _on_Medium_asteroid_area_entered(area):
	if area is Bullet:
		emit_signal("asteroid_destroyed", global_position)
		area.queue_free()
		queue_free()

func _on_Small_asteroid_area_entered(area):
	if area is Bullet:
		emit_signal("asteroid_destroyed", global_position)
		area.queue_free()
		queue_free()
